#!/usr/bin/python3

import re

file_name = "youtube1080p"

file = open(file_name, "r")
lines = file.readlines()
file.close()

out = open("output.csv", "w")
max_value = 10

for i in range(1, max_value + 1):
    re_client = re.compile('^' + str(i) + ' : c: ([0-9]*)$')
    re_rec = re.compile('^' + str(i) + ' : sr: ([0-9]*)$')
    re_bon = re.compile('^' + str(i) + ' : sb: ([0-9]*)$')
    re_temps = re.compile('^' + str(i) + ' : t: ([0-9]*)$')
    re_frequence = re.compile('^' + str(i) + ' : f: ([0-9]*)$')
    re_data = re.compile('^' + str(i) + ' : d: ([0-9]*)$')
    re_blanc = re.compile('^' + str(i) + ' : b: ([0-9]*)$')

    client = 0
    rec = 0
    bon = 0
    temps = 0
    frequence = 0
    data = 0
    blanc = 0
    for line in lines: 
        client1 = re_client.search(line)
        if client1:
            client = client1.group(1)
        
        rec1 = re_rec.search(line)
        if rec1:
            rec = rec1.group(1)

        bon1 = re_bon.search(line)
        if bon1:
            bon = bon1.group(1)

        temps1 = re_temps.search(line)
        if temps1:
            temps = temps1.group(1)

        frequence1 = re_frequence.search(line)
        if frequence1:
            frequence = frequence1.group(1)

        data1 = re_data.search(line)
        if data1:
            data = data1.group(1)

        blanc1 = re_blanc.search(line)
        if blanc1:
            blanc = blanc1.group(1)
    string = client + "," + rec + "," + bon + "," + temps + "," +\
        frequence + "," + data + "," + blanc + "\n"
    out.write(string)