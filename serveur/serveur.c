#define _GNU_SOURCE
#ifdef _WIN32
#include <windows.h>
#else
#include <fcntl.h>
#include <sys/mman.h>
#endif
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>
#include <stdint.h>
#include "../cacheutils.h"

// this number varies on different systems
#define MIN_CACHE_MISS_CYCLES (280)
#define FREQUENCE (10000000)
#define TIME_ENVOI (8000000)
#define TIME_RECEPTION (9000000)


size_t kpause = 0;
void flushandreload(void* addr)
{
  size_t time = rdtsc();
  maccess(addr);
  size_t delta = rdtsc() - time;
  flush(addr);
  if (delta < MIN_CACHE_MISS_CYCLES)
  {
    /* if (kpause > 0)
    {
      printf("%lu: Cache Hit (%lu cycles) after a pause of %lu cycles\n", time, delta, kpause);
    }
    kpause = 0;*/
    printf("1\n");
  }
  else {
    //kpause++;
    printf("0\n");
  }
  flush(addr);
}

void load(void* addr) {
  maccess(addr);
}

int main(int argc, char** argv)
{
  void* addr = &scanf;
  char paslu = 1;
  while(1) {
    size_t time = rdtsc() % FREQUENCE;
    if(time > TIME_RECEPTION && paslu){
      flushandreload(addr);
      paslu = 0;
    }
    if (time < TIME_RECEPTION) {
      paslu = 1;
    }
  }
  return 0;
}
