#define _GNU_SOURCE
#ifdef _WIN32
#include <windows.h>
#else
#include <fcntl.h>
#include <sys/mman.h>
#endif
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>
#include <stdint.h>
#include "utils/utils.h"
#include "utils/cacheutils.h"
#include "utils/constant.h"
#include "client/client.h"
#include "serveur/serveur.h"

// this number varies on different systems


int main(int argc, char *argv[])
{
  pid_t pid = fork();
  char* numero = argv[1];
  //long start = rdtsc();
  long start = time(NULL);
  if(!pid) {
    while(rdtsc() - start < START_TIME);
    while(rdtsc() % FREQUENCE < TIME_START);
    //client2(start, addr);
    unsigned int i = 0;
    while(time(NULL) - start < TOTAL_TIME_SECOND) {
      client3(DATA);
      i++;
    }
    // paquets envoyés
    printf("%s : c: %u\n", numero, i);
  } else {
    while(rdtsc() % FREQUENCE < TIME_START);
    //serveur(start, addr);
    unsigned int total = 0;
    unsigned int bon = 0;
    while(time(NULL) - start < TOTAL_TIME_SECOND + 1) {
      char a = (char) 0;
      serveur2(&a, start);
      if(a == 1) {
        total++;
        bon++;
      }
      if(a == 2) {
        total++;
      }
    }
    // paquets reçus
    printf("%s : sr: %u\n", numero, total);
    // paquets bons
    printf("%s : sb: %u\n", numero, bon);
    // constante temps
    printf("%s : t: %d\n", numero, TOTAL_TIME_SECOND);
    // constante fréquence
    printf("%s : f: %d\n", numero, FREQUENCE);
    // constante DATA
    printf("%s : d: %d\n", numero, TAILLE_DATA);
    // constante Blank
    printf("%s : b: %d\n", numero, NUMBER_BLANK);
  }
  return 0;
}

