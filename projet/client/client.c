#include <string.h>
#include "../utils/constant.h"
#include "../utils/cacheutils.h"
#include "../utils/utils.h"
#include "../utils/adresse.h"

/*void client(long start, void* addr) {
  char a = 1;
  char pasenvoye = 1;
  while(rdtsc() - start < TOTAL_TIME) {
    size_t time = rdtsc() % FREQUENCE;
    if (time > TIME_ENVOI) {
      if(a) {
        load(addr);
      } else {
        flush(addr);
      }
      pasenvoye = 1;
    } else {
      if (pasenvoye) {
        a = 1 - a;
        pasenvoye = 0;
      }
      flush(addr);
    }
  }
}

void client2(long start, void* addr) {
  char paquet[5];
  paquet[0] = 0b10101011;
  paquet[1] = 's';
  paquet[2] = 'a';
  paquet[3] = 'l';
  paquet[4] = 'u';
  //paquet[0] = 0b10101011;
  //paquet[1] = 0b11111111;
  //paquet[2] = 0b11111111;
  //paquet[3] = 0b11111111;
  //paquet[4] = 0b11111111;

  char a = 1;
  char pasenvoye = 1;

  int i = 0;
  int j = 0;
  while(rdtsc() - start < TOTAL_TIME) {
    if (j % 9 == 8 && i % 6 == 5) {
      j = 0;
      i = 0;
    }
    if (j % 9 == 8){
      j = 0;
      i++;
    }
    size_t time = rdtsc() % FREQUENCE;
    if (time > TIME_ENVOI) {
      char mask = 1 << (7-j); 
      if(paquet[i] & mask) {
        load(addr);
      } else {
        flush(addr);
      }
      pasenvoye = 1;
    } else {
      if (pasenvoye) {
        //char mask = 1 << (7-j); 
        //printf("client: %d\n", paquet[i] & mask);
        j++;
        a = 1 - a;
        pasenvoye = 0;
      }
      flush(addr);
    }
  }
}*/

void client3(char str[]) {
  void* addr = get_adresse();
  char paquet[TAILLE_PAQUET];
  for(int i = 0; i < NUMBER_BLANK; i++) {
    paquet[i] = 0b00000000;
  }
  paquet[NUMBER_BLANK] = 0b10101011; // Début de trame
  for(int i = 0; i < TAILLE_DATA; i++) {
     paquet[NUMBER_BLANK + i + 1] = str[i];
  }
  paquet[TAILLE_PAQUET - 1] = checksum(str);

  int i = 0;
  int j = 0;

  char pasenvoye = 0;

  // Test : affichage du paquet en binaire
  //affichage_binaire(paquet);

  while(i < TAILLE_PAQUET || j < 8) { // On envoi chaque bit
    if (j % 9 == 8) {
      j = 0;
      i++;
    }
    size_t time = rdtsc() % FREQUENCE;
    if (time > TIME_ENVOI) {
      char mask = 1 << (7-j);
      if(paquet[i] & mask) {
        load(addr);
        //if (!pasenvoye) {
        //  printf("1\n");
        //}
      } else {
        flush(addr);
        //if (!pasenvoye) {
        //  printf("0\n");
        //}
      }
      pasenvoye = 1;
    } else {
      if (pasenvoye) {
        //char mask = 1 << (7-j); 
        //printf("client: %d\n", paquet[i] & mask);
        //printf("%d\n", j);
        j++;
        pasenvoye = 0;
      }
      flush(addr);
    }
  }
}

void launch_client(char message[]) {
  char paquet[4];
  unsigned int i = 0;
  for(; i < strlen(message) ; i++) { // Parcourt l'ensemble du message
    paquet[i % 4] = message[i];
    if(i % 4 == 3 || i+1 == strlen(message)) { // Quand un paquet est plein ou fin de message
      //printf("paquet envoye\n");
      client3(paquet); // Envoi d'un paquet
      for(int j = 0 ; j < 4 ; j++){ // Reinitialisation du paquet
        paquet[j] = '\0';
      }
    }
  }
}

