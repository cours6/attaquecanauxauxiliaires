#include <stdio.h>
#include <time.h>

#include "../utils/constant.h"
#include "../utils/cacheutils.h"
#include "../utils/utils.h"
#include "../utils/adresse.h"

int nb_erreur = 0;
int nb_total = 0;
char a = 1;
/*
void serveur(long start, void* addr){
  char paslu = 1;
  while(rdtsc() - start < TOTAL_TIME) {
    size_t time = rdtsc() % FREQUENCE;
    if(time > TIME_RECEPTION && paslu){
      char test = flushandreload(addr);
      if(test != a ) {
        nb_erreur++;
      }
      nb_total++;
      paslu = 0;
      a = 1 - a;
    }
    if (time < TIME_RECEPTION) {
      paslu = 1;
    }
  }
  printf("total: %d, erreurs: %d\n", nb_total, nb_erreur);
}*/

enum etat {ATTENTE,
    PREMIER_BIT,SECOND_BIT,TROISIEME_BIT,QUATRIEME_BIT,
    CINQUIEME_BIT,SIZIEME_BIT,SEPTIEME_BIT,
    RECEPTION, FIN_LECTURE};

int serveur2(char* status, int start) {
  void* addr = get_adresse();
  enum etat e;
  e = ATTENTE;
  char paslu = 1;
  int nb_bit = 0;
  char data[TAILLE_DATA + 1];
  for(int i = 0; i < TAILLE_DATA + 1; i++) {
    data[i] = 0;
  }
  int i = 0;
  int j = 7;
  while(e != FIN_LECTURE && (time(NULL) - start < TOTAL_TIME_SECOND + 1)) {
    if (j == -1){
      j = 7;
      i++;
    }
    size_t time = rdtsc() % FREQUENCE;
    if(time > TIME_RECEPTION && paslu){
      char bit = flushandreload(addr);
      switch (e) {
        case ATTENTE:
          if(bit) {
            e = PREMIER_BIT;
          }
          break;
        case PREMIER_BIT:
          if(bit) {
            e = ATTENTE;
          } else {
            e = SECOND_BIT;
          }
          break;
        case SECOND_BIT:
          if(bit) {
            e = TROISIEME_BIT;
          } else {
            e = ATTENTE;
          }
          break;
        case TROISIEME_BIT:
          if(bit) {
            e = ATTENTE;
          } else {
            e = QUATRIEME_BIT;
          }
          break;
        case QUATRIEME_BIT:
          if(bit) {
            e = CINQUIEME_BIT;
          } else {
            e = ATTENTE;
          }
          break;
        case CINQUIEME_BIT:
          if(bit) {
            e = ATTENTE;
          } else {
            e = SIZIEME_BIT;
          }
          break;
        case SIZIEME_BIT:
          if(bit) {
            e = SEPTIEME_BIT;
          } else {
            e = ATTENTE;
          }
          break;
        case SEPTIEME_BIT:
          if(bit) {
            e = RECEPTION;
            //printf("Passage à l'état Reception!\n");
          } else {
            e = ATTENTE;
          }
          break;
        case RECEPTION:
          
          if(nb_bit >= 8 * (TAILLE_DATA + 1)){
            e = FIN_LECTURE;
            //printf("Passage à l'état Fin lecture!\n");
          } else {
            //printf("i: %d, j: %d, nb_bit: %d\n", i, j, nb_bit);
            
            if (bit) {
              //data[nb_bit >> 3] = (char) (data[nb_bit >> 3] | (1 << (nb_bit % 8)));
              data[i] = (char) (data[i] | (1 << j));
            }
            j--;
            nb_bit++;
          }
          
          break;
        case FIN_LECTURE:
          break;
      }
      /*if(bit) {
        printf("1\n");
      } else {
        printf("0\n");
      }*/
      
      //printf("%d\n", nb_bit);
      paslu = 0;
    }
    if (time < TIME_RECEPTION) {
      paslu = 1;
    }
  }
  //printf("%d\n", *data);
  //affichage_binaire(data);
  if(data[TAILLE_DATA] == checksum(data)) {
    *status = 1;
    //printf("bon\n");
  } else {
    *status = 2;
    //printf("mauvais\n");
  }

  return 0;
}


