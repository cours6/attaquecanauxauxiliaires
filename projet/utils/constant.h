#ifndef CONSTANT_H
#define CONSTANT_H

#define FREQUENCE (7500)
#define TIME_ENVOI (0.5 * FREQUENCE)
#define TIME_RECEPTION (0.8 * FREQUENCE)
#define TIME_START (0.1 * FREQUENCE)
#define START_TIME (10 * FREQUENCE)
#define TOTAL_TIME_SECOND (30)

//Paquet related constants
#define NUMBER_BLANK (0)
#define TAILLE_DATA (4)
#define DATA "salutGuenaeladf"
#define TAILLE_PAQUET (NUMBER_BLANK + TAILLE_DATA + 2)

#endif