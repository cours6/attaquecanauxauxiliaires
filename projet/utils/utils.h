#ifndef UTILS_H
#define UTILS_H
#define MIN_CACHE_MISS_CYCLES (200)

char flushandreload(void* addr);

void load(void* addr);

char checksum(char str[]);

void affichage_binaire(char paquet[]);

#endif