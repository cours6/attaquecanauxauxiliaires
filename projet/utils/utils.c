#include <stdio.h>
#include "cacheutils.h"
#include "utils.h"
#include "constant.h"


char flushandreload(void* addr)
{
  size_t time = rdtsc();
  maccess(addr);
  size_t delta = rdtsc() - time;
  //flush(addr);
  if (delta < MIN_CACHE_MISS_CYCLES)
  {
    //printf("serveur: 1\n");
    return (char) 1;
    
  }
  else {
    //printf("serveur: 0\n");
    return (char) 0;
  }
}

void load(void* addr) {
  maccess(addr);
}

char checksum(char str[]) {
  char res = 0;
  for(int i = 0; i < TAILLE_DATA; i++) {
    res = res ^ str[i];
  }
  return res;
}

void affichage_binaire(char paquet[]) {
  char mask = 0; 
  printf("Debut affichage paquet\n");
  for(int i = 0 ; i < TAILLE_PAQUET ; i++) { // Parcourt de chaque octet du paquet
    for(int j = 0 ; j < 8 ; j++) { // Affichage de chaque bit
      mask = 1 << (7-j); 
      if((paquet[i] & mask) == -128) {
        printf("1");
      } else {
        printf("%d", (paquet[i] & mask) >> (7-j));
      }
      mask = 0;
    }
    printf(" - ");
  }
  printf("\nFin affichage paquet\n");
}
