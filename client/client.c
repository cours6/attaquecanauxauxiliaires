#define _GNU_SOURCE
#ifdef _WIN32
#include <windows.h>
#else
#include <fcntl.h>
#include <sys/mman.h>
#endif
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>
#include <stdint.h>
#include "../cacheutils.h"

// this number varies on different systems
#define MIN_CACHE_MISS_CYCLES (280)
#define FREQUENCE (10000000)
#define TIME_ENVOI (8000000)
#define TIME_RECEPTION (9000000)


size_t kpause = 0;
void flushandreload(void* addr)
{
  size_t time = rdtsc();
  maccess(addr);
  size_t delta = rdtsc() - time;
  flush(addr);
  if (delta < MIN_CACHE_MISS_CYCLES)
  {
    printf("1\n");
  }
  else {
    printf("0\n");
  }
  flush(addr);
}

void load(void* addr) {
  maccess(addr);
}

int main(int argc, char** argv)
{
  void* addr = &scanf;
  char a = 1;
  char pasenvoye = 1;
  while(1) {
    size_t time = rdtsc() % FREQUENCE;
    if (time > TIME_ENVOI) {
      if(a) {
        load(addr);
      } else {
        flush(addr);
      }
      pasenvoye = 1;
    } else {
      if (pasenvoye) {
        a = 1 - a;
        pasenvoye = 0;
      }
      flush(addr);
    }
  }
  return 0;
}
